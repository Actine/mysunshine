package com.actinarium.sunshine.app;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class ForecastAdapter extends CursorAdapter {

    private boolean isTablet;

    private static final int VIEW_TYPE_TODAY = 0;
    private static final int VIEW_TYPE_FUTURE_DAY = 1;

    public ForecastAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        isTablet = context.getResources().getBoolean(R.bool.isTablet);
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0 && !isTablet) ? VIEW_TYPE_TODAY : VIEW_TYPE_FUTURE_DAY;
    }

    @Override
    public int getViewTypeCount() {
        return isTablet ? 1 : 2;
    }

    /**
     * Copy/paste note: Replace existing newView() method in ForecastAdapter with this one.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // Choose the layout type
        int viewType = getItemViewType(cursor.getPosition());
        int layoutId = viewType == VIEW_TYPE_TODAY ? R.layout.list_item_forecast_today : R.layout.list_item_forecast;
        View view = LayoutInflater.from(context).inflate(layoutId, parent, false);
        view.setTag(new ViewHolder(view));
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder holder = (ViewHolder) view.getTag();

        int weatherId = cursor.getInt(ForecastFragment.COL_WEATHER_ID);
        holder.iconView.setImageResource(R.drawable.weather);

        String dateString = cursor.getString(ForecastFragment.COL_WEATHER_DATE);
        holder.dateView.setText(Util.getFriendlyDayString(context, dateString));

        String description = cursor.getString(ForecastFragment.COL_WEATHER_DESC);
        holder.descriptionView.setText(description);

        double high = cursor.getDouble(ForecastFragment.COL_WEATHER_MAX_TEMP);
        final String highTempText = Util.formatTemperature(context, high);
        holder.highTempView.setText(highTempText);
        holder.highTempView.setContentDescription("High temperature is " + highTempText);

        double low = cursor.getDouble(ForecastFragment.COL_WEATHER_MIN_TEMP);
        final String lowTempText = Util.formatTemperature(context, low);
        holder.lowTempView.setText(lowTempText);
        holder.lowTempView.setContentDescription("Low temperature is " + lowTempText);

        final int iconResource;
        if (getItemViewType(cursor.getPosition()) == VIEW_TYPE_TODAY) {
            iconResource = Util.getArtResourceForWeatherCondition(cursor.getInt(ForecastFragment.COL_WEATHER_ICON));
        } else {
            iconResource = Util.getIconResourceForWeatherCondition(cursor.getInt(ForecastFragment.COL_WEATHER_ICON));
        }
        holder.iconView.setImageResource(iconResource);
    }

    /**
     * Cache of the children views for a forecast list item.
     */
    public static class ViewHolder {
        public final ImageView iconView;
        public final TextView dateView;
        public final TextView descriptionView;
        public final TextView highTempView;
        public final TextView lowTempView;

        public ViewHolder(View view) {
            iconView = (ImageView) view.findViewById(R.id.list_item_icon);
            dateView = (TextView) view.findViewById(R.id.list_item_date_textview);
            descriptionView = (TextView) view.findViewById(R.id.list_item_forecast_textview);
            highTempView = (TextView) view.findViewById(R.id.list_item_high_textview);
            lowTempView = (TextView) view.findViewById(R.id.list_item_low_textview);
        }
    }
}
