package com.actinarium.sunshine.app;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.actinarium.sunshine.app.data.WeatherContract;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final String DATE_ARG = "date";

    private ShareActionProvider shareActionProvider;

    private String dateString;
    private static int DETAIL_LOADER = 1;

    private TextView niceDateTextView;
    private TextView dateTextView;
    private TextView forecastTextView;
    private TextView tempLowTextView;
    private TextView tempHighTextView;
    private TextView humidityTextView;
    private TextView windTextView;
    private DirectionArrowView windDirectionView;
    private TextView pressureTextView;
    private ImageView iconView;

    public static DetailFragment newInstance(String date) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putString(DATE_ARG, date);
        fragment.setArguments(args);
        return fragment;
    }

    public DetailFragment() {
        setHasOptionsMenu(true);
    }

    private String getDateString() {
        if (dateString == null) {
            dateString = getArguments().getString(DATE_ARG);
        }
        return dateString;
    }

    public static final String[] FORECAST_COLUMNS = {
            WeatherContract.WeatherEntry.TABLE_NAME + "." + WeatherContract.WeatherEntry._ID,
            WeatherContract.WeatherEntry.COLUMN_DATETEXT,
            WeatherContract.WeatherEntry.COLUMN_SHORT_DESC,
            WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
            WeatherContract.WeatherEntry.COLUMN_MIN_TEMP,
            WeatherContract.WeatherEntry.COLUMN_HUMIDITY,
            WeatherContract.WeatherEntry.COLUMN_WIND_SPEED,
            WeatherContract.WeatherEntry.COLUMN_DEGREES,
            WeatherContract.WeatherEntry.COLUMN_PRESSURE,
            WeatherContract.WeatherEntry.COLUMN_WEATHER_ID,
            WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING
    };

    public static final int COL_WEATHER_ID = 0;
    public static final int COL_WEATHER_DATE = 1;
    public static final int COL_WEATHER_DESC = 2;
    public static final int COL_WEATHER_MAX_TEMP = 3;
    public static final int COL_WEATHER_MIN_TEMP = 4;
    public static final int COL_HUMIDITY = 5;
    public static final int COL_WIND_SPEED = 6;
    public static final int COL_DEGREES = 7;
    public static final int COL_PRESSURE = 8;
    public static final int COL_WEATHER_CONDITION = 9;
    public static final int COL_LOCATION_SETTING = 10;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getDateString() != null) {
            getLoaderManager().initLoader(DETAIL_LOADER, null, this);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(menu.findItem(R.id.action_share));

        String text = getDateString();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.putExtra(Intent.EXTRA_TEXT, text + " #SunshineApp");
        intent.setType("text/plain");
        setShareIntent(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        niceDateTextView = (TextView) rootView.findViewById(R.id.detail_date_textview_nice);
        dateTextView = (TextView) rootView.findViewById(R.id.detail_date_textview);
        forecastTextView = (TextView) rootView.findViewById(R.id.detail_forecast_textview);
        tempHighTextView = (TextView) rootView.findViewById(R.id.detail_high_textview);
        tempLowTextView = (TextView) rootView.findViewById(R.id.detail_low_textview);
        humidityTextView = (TextView) rootView.findViewById(R.id.detail_humidity);
        windTextView = (TextView) rootView.findViewById(R.id.detail_wind);
        windDirectionView = (DirectionArrowView) rootView.findViewById(R.id.detail_wind_arrow);
        pressureTextView = (TextView) rootView.findViewById(R.id.detail_pressure);
        iconView = (ImageView) rootView.findViewById(R.id.detail_icon);

        return rootView;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String location = Util.getPreferredLocation(getActivity());
        Uri weatherItemUri = WeatherContract.WeatherEntry.buildWeatherLocationWithDate(location, getDateString());
        return new CursorLoader(
                getActivity(),
                weatherItemUri,
                FORECAST_COLUMNS,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        data.moveToFirst();
        niceDateTextView.setText(Util.getDayName(getActivity(), data.getString(COL_WEATHER_DATE)));
        dateTextView.setText(Util.getFormattedMonthDay(getActivity(), data.getString(COL_WEATHER_DATE)));
        forecastTextView.setText(data.getString(COL_WEATHER_DESC));
        tempLowTextView.setText(Util.formatTemperature(getActivity(), data.getDouble(COL_WEATHER_MIN_TEMP)));
        tempHighTextView.setText(Util.formatTemperature(getActivity(), data.getDouble(COL_WEATHER_MAX_TEMP)));
        humidityTextView.setText(getString(R.string.format_humidity, data.getDouble(COL_HUMIDITY)));
        final double windDirectionAngle = data.getDouble(COL_DEGREES);
        windTextView.setText(Util.getFormattedWind(getActivity(), data.getDouble(COL_WIND_SPEED), windDirectionAngle));
        windDirectionView.setAngle((float) windDirectionAngle + 180);
        pressureTextView.setText(getString(R.string.format_pressure, data.getDouble(COL_PRESSURE)));
        iconView.setImageResource(Util.getArtResourceForWeatherCondition(data.getInt(COL_WEATHER_CONDITION)));
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDateString() != null) {
            getLoaderManager().restartLoader(DETAIL_LOADER, null, this);
        }
    }

    private void setShareIntent(Intent shareIntent) {
        if (shareActionProvider != null) {
            shareActionProvider.setShareIntent(shareIntent);
        }
    }
}
