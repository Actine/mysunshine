package com.actinarium.sunshine.app;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class DirectionArrowView extends View {

    public static String ARROW_TEXT = "\u2191";

    private float mAngle;
    private Path mPath;
    private Paint mPaint;
    private int mRealDim;

    public DirectionArrowView(Context context) {
        super(context);
    }

    public DirectionArrowView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DirectionArrowView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.DirectionArrowView,
                0, 0);

        try {
            mAngle = a.getFloat(R.styleable.DirectionArrowView_angle, 0f);
        } finally {
            a.recycle();
        }
        if (getImportantForAccessibility() == IMPORTANT_FOR_ACCESSIBILITY_AUTO) {
            setImportantForAccessibility(IMPORTANT_FOR_ACCESSIBILITY_YES);
        }
    }

    @TargetApi(21)
    public DirectionArrowView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public float getAngle() {
        return mAngle;
    }

    public void setAngle(float angle) {
        mAngle = angle;
        invalidate();
        requestLayout();
        // Pre-calculate all values
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        // Square dimensions calculated out of given specs

        if (widthMode == MeasureSpec.UNSPECIFIED && heightMode == MeasureSpec.UNSPECIFIED) {
            mRealDim = 100;
        } else if (widthMode == MeasureSpec.UNSPECIFIED) {
            mRealDim = height;
        } else if (heightMode == MeasureSpec.UNSPECIFIED) {
            mRealDim = width;
        } else {
            mRealDim = Math.min(width, height);
        }

        setMeasuredDimension(mRealDim, mRealDim);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mPath == null) {
            init();
        }
        canvas.drawPath(mPath, mPaint);
        canvas.drawTextOnPath(ARROW_TEXT, mPath, 0, 0, mPaint);
    }

    private void init() {
        final int halfDim = mRealDim / 2;
        Matrix matrix = new Matrix();
        matrix.postRotate(mAngle, halfDim, halfDim);
        mPath = new Path();
        mPath.moveTo(0, mRealDim * 0.75f);
        mPath.rLineTo(mRealDim, 0);
        mPath.transform(matrix);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(Color.BLACK);
        mPaint.setTypeface(Typeface.DEFAULT);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTextSize(mRealDim * 0.75f);
    }

    @Override
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent event) {
        event.getText().add("Wind direction is " + mAngle + " degrees");
        return true;
    }

    @Override
    @TargetApi(14)
    public void onPopulateAccessibilityEvent(AccessibilityEvent event) {
        event.getText().add("Wind direction is " + mAngle + " degrees");
    }
}
