package com.actinarium.sunshine.app;

import android.test.suitebuilder.TestSuiteBuilder;
import junit.framework.Test;

/**
 * <p></p>
 *
 * @author Paul Danyliuk
 * @version $Id$
 */
public class FullTestSuite {
    public static Test suite() {
        return new TestSuiteBuilder(FullTestSuite.class)
                .includeAllPackagesUnderHere().build();
    }

    public FullTestSuite() {
        super();
    }
}
